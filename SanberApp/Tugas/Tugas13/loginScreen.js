import React from 'react';
import {
  StyleSheet,
  View, 
  Image,
  Text,
  TextInput,
  TouchableOpacity
}from 'react-native';


export default class App extends React.Component{
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Image source={require('./images/logo.png')}/>
        </View>
        <View style={styles.loginContainer}>
          <Text style={styles.loginText}>Login</Text>
        </View>
        <View style={styles.usernameContainer}>
          <Text style={styles.miniText}>username/email</Text>
          <TextInput style={styles.inputbox} />
        </View>
        <View style={styles.passwordContainer}>
          <Text style={styles.miniText}>password</Text>
          <TextInput style={styles.inputbox} />
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.buttonMasuk}>
            <Text style={styles.buttonText}>Masuk</Text>
          </TouchableOpacity>
          <Text style={{fontSize: 20, color:'#3EC6FF', marginVertical:8}}> atau </Text>
          <TouchableOpacity style={styles.buttonDaftar}>
            <Text style={styles.buttonText}>Daftar?</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1
  },
  logoContainer:{
    paddingTop: 45
  },
  buttonContainer: {
    alignItems: 'center',
    marginTop: 16    
  },
  loginContainer:{
    alignItems: 'center',
    marginVertical: 28
  },
  buttonMasuk:{
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: '#003366',
    width: 120,
    height: 35
  },
  buttonDaftar:{
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: '#3EC6FF',
    width: 120,
    height: 35
  },
  buttonText : {
    fontSize: 20,
    color: 'white',
  },
  loginText:{
    fontSize: 24,
    color: '#003366'
  },
  inputbox : { 
    height: 40, 
    borderColor: '#003366', 
    borderWidth: 1,
  },
  miniText: {
    fontSize: 11
  }, 
  usernameContainer:{
    marginBottom: 16,
    paddingHorizontal: 40
  },
  passwordContainer:{
    marginBottom: 16,
    paddingHorizontal: 40
  }
});