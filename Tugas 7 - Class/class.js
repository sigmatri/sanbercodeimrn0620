//NAMA      : TRIAN ANNAS
//gitlab    : @sigmatri
//telegram  : 08986174086



//SOAL 1
//Release 0
class Animal {
    // Code class di sini
    constructor(name){
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}
 
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false


//Release 1
// Code class Ape dan class Frog di sini
class Ape extends Animal{
    constructor(name){
        super(name)
        this.legs = 2
    }

    yell() {
        console.log("Auooo")
    }
}

class Frog extends Animal{
    constructor(name){
        super(name)
        this.cold_blooded = true
    }

    jump(){
        console.log("hop hop")
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
 
var kodok = new Frog("buduk")
kodok.jump() // "hop hop" 


//SOAL 2
class Clock {
    // Code di sini
    timer;
    constructor({template}){
        this.template = template
    }

    static render(template){
        let date = new Date();

        let hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        let mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        let secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
        
        let output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);

        console.log(output);
    }
    start(){
        let template = this.template
        this.timer = setInterval(function(){Clock.render(template)}, 1000);
    }
    stop(){
        clearInterval(this.timer);
    }
}

var clock = new Clock({template: 'h:m:s'});
clock.start();