//NAMA      : TRIAN ANNAS
//gitlab    : @sigmatri
//telegram  : 08986174086


//SOAL 1
//WHILE
console.log("LOOPING PERTAMA")
var genap = 2
while(genap <= 20){
    var output = genap + ' - I love coding'
    console.log(output)
    genap = genap +2
}
console.log("LOOPING KEDUA")
var genap = 20
while(genap > 0){
    var output = genap + ' - I love coding'
    console.log(output)
    genap = genap -2
}

console.log("\n")
//SOAL 2
//FOR
for (var i =1; i<=20; i++){
    if ((i % 2) == 0){//genap
        var output = i + " - Berkualitas"
    }else if(((i % 2) == 1)&&(i % 3 == 0)){//ganjil dan kelipatan 3
        var output = i + " - I Love Coding"
    }else if((i % 2) == 1){//ganjil
        var output = i + " - Santai"
    }
    console.log(output)
}


console.log("\n")
//SOAL 3
//PERSEGI PANJANG 8 x 4
for (var i = 0; i<4; i++){
    var output= "########"
    console.log(output)
}

console.log("\n")
//SOAL 4
//Membuat Tangga, dimana tinggi = 7
for (var i = 0; i<7; i++){
    var output= ""
    for(var j = 0; j<=i; j++){
        output = output + "#"
    }
    console.log(output)
}

console.log("\n")
//SOAL 5
//Membuat Papan Catur 8 x 8
for (var i = 0; i<8; i++){
    if (i % 2 == 0){
        var output = " # # # #"
    }else{
        var output = "# # # # "
    }
    console.log(output)
}