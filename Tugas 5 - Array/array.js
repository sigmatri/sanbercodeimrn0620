//NAMA      : TRIAN ANNAS
//gitlab    : @sigmatri
//telegram  : 08986174086


//SOAL 1
function range(startNum, finishNum) {
    if(startNum!= null && finishNum!=null){
        var array = []
        if(startNum < finishNum){
            for(var i= startNum; i<=finishNum; i++){
                array.push(i);
            }
        }else{
            for(var i= startNum; i>=finishNum; i--){
                array.push(i);
            }
        }
        return array;
    }else{
        return -1
    }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("\n")
//SOAL 2
function rangeWithStep(startNum, finishNum, step=1) {
    if(startNum!= null && finishNum!=null){
        var array = []
        if(startNum < finishNum){
            for(var i= startNum; i<=finishNum; i = i + step){
                array.push(i);
            }
        }else{
            for(var i= startNum; i>=finishNum; i= i - step){
                array.push(i);
            }
        }
        return array;
    }else{
        return -1
    }
}


console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 


console.log("\n")
//SOAL 3
function sum(startNum, finishNum, step=1){
    if(startNum!= null && finishNum!=null){
        var jumlah = 0
        if(startNum < finishNum){
            for(var i= startNum; i<=finishNum; i = i + step){
                jumlah = jumlah + i;
            }
        }else{
            for(var i= startNum; i>=finishNum; i= i - step){
                jumlah = jumlah + i;
            }
        }
        return jumlah;
    }else{
        if (startNum != null){
            return startNum
        }else{
            return 0
        }
    }
}



console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0


console.log("\n")
//SOAL 4
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ] 


function dataHandling(input){
    if(input != null){
        var string1 =''
        input.forEach(element => {
            var string2 = `Nomor ID:  ${element[0]}\nNama Lengkap:  ${element[1]}\nTTL:  ${element[2]} ${element[3]}\nHobi:  ${element[4]}\n\n`
            string1 = string1 + string2 
        });
    }
    return string1;
}

console.log(dataHandling(input))



console.log("\n")
//SOAL 5
function balikKata(kata){
    if(kata != null){
        var newKata = ''
        for(var i=kata.length-1; i>=0; i--){
            newKata = newKata + kata[i]
        }
        return newKata
    }
    return -1
}


console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 




console.log("\n")
//SOAL 6

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]  


function dataHandling2(input){
    input[1] = input[1] + ' Elsharawy'
    input[2] = 'Provinsi ' + input[2]
    input.splice(4, 1)
    input.splice(4,0,'Pria')
    input.splice(5,0,'SMA Internasional Metro')
    
    console.log(input)

    var tanggal = input[3].split('/')
    var bulan_str = ''
    switch(parseInt(tanggal[1])){
        case 1: {
                    bulan_str = "Januari";
                    break;            
                }
        case 2: {
                    bulan_str = "Februari";
                    break;            
                }
        case 3: {
                    bulan_str = "Maret";
                    break;            
                }
        case 4: {
                    bulan_str = "April";
                    break;            
                }
        case 5: {
                    bulan_str = "Mei";
                    break;            
                }
        case 6: {
                    bulan_str = "Juni";
                    break;            
                }
        case 7: {
                    bulan_str = "Juli";
                    break;            
                }
        case 8: {
                    bulan_str = "Agustus";
                    break;            
                }
        case 9: {
                    bulan_str = "September";
                    break;            
                }
        case 10: {
                    bulan_str = "Oktober";
                    break;            
                }
        case 11: {
                    bulan_str = "November";
                    break;            
                }
        case 12: {
                    bulan_str = "Desember";
                    break;            
                }
    }

    console.log(bulan_str)
    tanggal = tanggal.sort(function (value1, value2) { return value1 < value2 })
    console.log(tanggal)
    tanggal = tanggal.join('-')
    console.log(tanggal)

    if(input[1].length > 15){
        input[1] = input[1].slice(0,15)
    }
    console.log(input[1])
}

dataHandling2(input)