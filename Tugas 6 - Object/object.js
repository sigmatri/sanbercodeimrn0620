//NAMA      : TRIAN ANNAS
//gitlab    : @sigmatri
//telegram  : 08986174086

//SOAL 1
console.log("\nnomor 1")
function arrayToObject(arr) {
    if(arr!=null){
        if(arr.length != 0){
            let i = 1
            arr.forEach(element => {
                let first_name = element[0]
                let second_name = element[1]
                let gender = element[2]
                let birth = null
                let age = null
                if(element[3]!=null){
                    birth = element[3]
                    let now = new Date()
                    if(now.getFullYear()>birth){
                        age = now.getFullYear() - birth
                    }else{
                        age = `"Invalid Birth Year"`    
                    }
                }else{
                    age = `"Invalid Birth Year"`
                }
                let object = `{
                    firtName : "${first_name}",
                    lastName : "${second_name}",
                    gender : "${gender}",
                    age : ${age}
                }`
                let string = `${i}. ${first_name} ${second_name}: ${object}`
                i++
                console.log(string)
            });
        }else{
            console.log("")
        }
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 

var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 

// Error case 
arrayToObject([]) // ""


//SOAL 2
console.log("\nnomor 2")
function shoppingTime(memberId, money) {
    if(memberId != null && money!= null){
        if(memberId==''){
            return "Mohon maaf, toko X hanya berlaku untuk member saja"
        }else{
            if(money<50000){
                return "Mohon maaf, uang tidak cukup"
                
            }else{
                let listPurchased = []
                let uang = money
                if(money >= 1500000){
                    money = money - 1500000
                    listPurchased.push("Sepatu brand Stacattu")
                }
                if(money >= 500000){
                    money = money - 500000
                    listPurchased.push("Baju brand Zoro")
                }
                if(money >= 250000){
                    money = money - 250000
                    listPurchased.push("Baju brand H&N")
                }
                if(money >= 175000){
                    money = money - 175000
                    listPurchased.push("Sweater brand Uniklooh")
                }
                if(money >= 50000){
                    money = money - 50000
                    listPurchased.push("Casing Handphone")
                }
                let changeMoney = money
                let listPurchased_str=''
                let j=1
                listPurchased.forEach(e=>{
                    if(j!=listPurchased.length){
                        listPurchased_str = listPurchased_str +` '${e}',\n`
                    }else{
                        listPurchased_str = listPurchased_str +` '${e}'`
                    }
                    j++
                })
                //anggap output sebagai object, karena keluaran berupa string maka tampilan disesuaikan dengan tampilan object(seperti JSON)
                let output = `{ memberId: ${memberId},\nmoney : ${uang},\nlistPurchased: \n[${listPurchased_str} ],\nchangeMoney: ${changeMoney}}`;
                return (output)
            }
        }
    }else{
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }
}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja


console.log("\nnomor 3")
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    if(arrPenumpang!= null){
        if(arrPenumpang.length!=0){
            let string = ''
            let count = 1
            arrPenumpang.forEach(e =>{
                let awal =0
                let akhir =0
                for(let i = 0; i< rute.length; i++){
                    if(rute[i]==e[1]){
                        awal =i;
                    }
                    if(rute[i]==e[2]){
                        akhir =i;
                    }
                }
                let bayar = 2000 * (akhir - awal)
                let object = `{ penumpang : '${e[0]}', naikDari : '${e[1]}', tujuan:'${e[2]}', bayar: ${bayar} }`
                if(count!=arrPenumpang.length){
                    string = string + `${object},\n`
                }else{
                    string = string + `${object}`
                }
                count++
            });
            let output = `[ ${string} ]`
            return output            
        }else{
            return '[]'
        }
    }
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]