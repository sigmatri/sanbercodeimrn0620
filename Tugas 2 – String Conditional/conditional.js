//NAMA      : TRIAN ANNAS
//gitlab    : @sigmatri
//telegram  : 08986174086



//if-else
var nama = "trian"
var peran = ""

//asumsi input peran hanya ada 4, stringkosong, penyihir, guard dan werewolf, selain itu akan diabaikan
if (nama == ""){
    console.log("Nama harus diisi!")
}else{
    if (peran==""){
        var wellcome = "Halo " + nama +" Pilih Peranmu untuk memulai game!"
        console.log(wellcome)
    }else if (peran=="Penyihir" || peran=="penyihir"){
        var wellcome1 = "Selamat datang di Dunia Werewolf, "+ nama
        var wellcome = "Halo Penyihir " + nama +", kamu dapat melihat siapa yang menjadi werewolf!"
        console.log(wellcome1)
        console.log(wellcome)
    }else if (peran=="Guard" || peran=="guard"){
        var wellcome1 = "Selamat datang di Dunia Werewolf, "+ nama
        var wellcome = "Halo Guard " + nama +", kamu akan membantu melindungi temanmu dari serangan werewolf."
        console.log(wellcome1)
        console.log(wellcome)
    }else if (peran=="Werewolf" || peran=="werewolf"){
        var wellcome1 = "Selamat datang di Dunia Werewolf, "+ nama
        var wellcome = "Halo Werewolf " + nama +", Kamu akan memakan mangsa setiap malam!"
        console.log(wellcome1)
        console.log(wellcome)
    }
    
}

console.log("\n")
//switch case
var hari = 21; 
var bulan = 1; 
var tahun = 1945;



var bulan_str=""
switch(bulan){
    case 1: {
                bulan_str = "Januari";
                break;            
            }
    case 2: {
                bulan_str = "Februari";
                break;            
            }
    case 3: {
                bulan_str = "Maret";
                break;            
            }
    case 4: {
                bulan_str = "April";
                break;            
            }
    case 5: {
                bulan_str = "Mei";
                break;            
            }
    case 6: {
                bulan_str = "Juni";
                break;            
            }
    case 7: {
                bulan_str = "Juli";
                break;            
            }
    case 8: {
                bulan_str = "Agustus";
                break;            
            }
    case 9: {
                bulan_str = "September";
                break;            
            }
    case 10: {
                bulan_str = "Oktober";
                break;            
            }
    case 11: {
                bulan_str = "November";
                break;            
            }
    case 12: {
                bulan_str = "Desember";
                break;            
            }
}
tanggal = hari + " " + bulan_str + " " + tahun
console.log(tanggal);

//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 