import React from 'react';
import { StyleSheet, View, StatusBar} from 'react-native';

import Quiz from './Quiz 3/index'

export default function App(){
  return (
    <View style={styles.container}>
      <StatusBar/>
      <Quiz/>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
