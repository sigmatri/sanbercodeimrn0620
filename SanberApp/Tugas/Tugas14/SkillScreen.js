import React from 'react';
import { 
  View, 
  Text, 
  TouchableOpacity, 
  Image,
  StyleSheet,
  FlatList
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import SkillItem from './components/SkillItem';
import skillData from './skillData.json';

export default class SkillScreen extends React.Component{
  render(){
    return(
      <View style={styles.container}>
        <View style={styles.iconBar}>
          <Image source={require('./images/logo.png') } style={{width: 180, height:51}}/>
        </View>
        <View style={styles.profileBar}>
          <Icon name="account-circle" size={35} color={'#3EC6FF'}/>
          <View style={StyleSheet.profileStatus}>
            <Text style={styles.text_1}>Hai,</Text>
            <Text style={styles.text_2}>Mukhlis Hanafi</Text>
          </View>
        </View>
        <View style={styles.header}>
          <Text style={styles.headerText}>SKILL</Text>
        </View>
        <View style={styles.delimiter}/>
        <View style={styles.tabBar}>
          <TouchableOpacity style={styles.TabItem}>
            <Text style={styles.tabText}>Libary/Framework</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.TabItem}>
            <Text style={styles.tabText}>Bahasa Pemprograman</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.TabItem}>
            <Text style={styles.tabText}>Teknologi</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.body}>
          <FlatList 
            data={skillData.items}
            renderItem={(skill)=><SkillItem skill={skill.item}/>}
            keyExtractor={(item)=>item.id}
          />
        </View>  
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1
  },
  iconBar:{
    alignItems:'flex-end'
  },
  profileBar:{
    paddingHorizontal: 20,
    paddingVertical: 10,
    flexDirection: 'row',
  },
  text_1:{
    fontSize:12,
    lineHeight:14,
  },
  text_2:{
    fontSize:16,
    lineHeight:19,
    color:'#003366',
  },
  header:{
    paddingHorizontal: 20,
  },
  headerText:{
    fontSize:36,
    lineHeight:42,
    color:'#003366'
  },
  delimiter:{
    marginHorizontal:20,
    height:2,
    backgroundColor:'#3EC6FF'
  },
  tabBar:{
    flexDirection:'row',
    paddingHorizontal:20,
    justifyContent:'space-between',
    marginVertical:10
  },
  TabItem:{
    backgroundColor:'#B4E9FF',
    borderRadius:8,

  },
  tabText:{
    margin:8,
    fontSize:11,
    color:'#003366'
  },
  body:{
    flex: 1
  },
});