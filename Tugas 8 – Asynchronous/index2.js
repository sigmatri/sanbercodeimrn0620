//NAMA      : TRIAN ANNAS
//gitlab    : @sigmatri
//telegram  : 08986174086

var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'Kalkulus', timeSpent: 4000}
]

var index = 0
function bacaBuku(time, book){
    readBooksPromise(time, book)
        .then(function (sisawaktu) {
            index++;
            if(index<books.length){
                bacaBuku(sisawaktu, books[index])
            }else{
                console.log('tidak ada buku yang tersisa')
            }
        })
        .catch(function (error) {
            console.log(`sisa waktunya adalah ${error+books[index].timeSpent}`)
        });
}

bacaBuku(10000, books[index])